let btnInject = document.getElementById('btn-inject');
let divOutput = document.getElementById('div-output');

btnInject.onclick = function(element) {
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {execute: true}, function(response) {
            if(response === undefined) {
                return;
            }
            let node = document.createElement('div');
            let pNode = document.createElement('p');
            divOutput.appendChild(node);
            node.appendChild(pNode);

            if(response.success) {
                node.classList.add('success');
                pNode.innerText = 'Scripts successfully poisoned:';
                let ulNode = document.createElement('ul');
                node.appendChild(ulNode);
                for(url of response.poisoned) {
                    let liNode = document.createElement('li');
                    liNode.innerText = url;
                    ulNode.appendChild(liNode);
                }
            } else {
                node.classList.add('fail');
                pNode.innerText = response.message;
            }
        });
    });
    btnInject.disabled = true;
};